import XCTest

import MyFirstSwiftLibTests

var tests = [XCTestCaseEntry]()
tests += MyFirstSwiftLibTests.allTests()
XCTMain(tests)
