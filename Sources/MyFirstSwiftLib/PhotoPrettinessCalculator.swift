import CoreImage
import UIKit

public struct PhotoPrettinessCalculator {
   public static func findPhotoPrettiness(image: UIImage) -> String {
        
        let userImage = CIImage(image:image)
        
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: userImage!)  as! [CIFaceFeature]

        let prettiness = ["very low" , "low" , "medium" , "high" , "very high"]
        var prettinessLevel = 0
        
        if let face = faces.first {
            if face.hasMouthPosition {prettinessLevel+=1}
            if face.hasLeftEyePosition && face.hasRightEyePosition {prettinessLevel+=1}
            if !face.rightEyeClosed && !face.leftEyeClosed {prettinessLevel+=1}
            if face.hasSmile {prettinessLevel+=1}
            if face.faceAngle > 10 || face.faceAngle < -10  {prettinessLevel+=1}
        }
        return prettiness[prettinessLevel]
        
    }
}
